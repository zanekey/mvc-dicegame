package client;

import controller.PlayerController;
import model.GameEngineImpl;
import view.GameEngineCallbackGUI;
import view.MainFrame;

import javax.swing.*;

public class Driver {


    public static void main(String[] args)
    {
        /* instantiate fields */
        GameEngineImpl model  = new GameEngineImpl() ;
        MainFrame view = new MainFrame() ;
        GameEngineCallbackGUI gameEngineCallbackGUI = new GameEngineCallbackGUI(view);
        /* add gameEngineCallBack */
        model.addGameEngineCallback(gameEngineCallbackGUI);

        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                /* instantiate controller and run project */
                new PlayerController(model , view);
            }
        });
    }

}
