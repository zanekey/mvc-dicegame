package model;

import model.interfaces.DicePair;
import model.interfaces.Player;

public class SimplePlayer implements Player
{
	/* init fields */
	private String playerId;
	private String playerName;
	private int points;
	private int bet;
	private DicePair dice;
	/* constructor */
	public SimplePlayer(String playerId, String playerName, int initialPoints)
	{
		this.playerId = playerId;
		this.playerName = playerName;
		this.points = initialPoints;
		this.points = initialPoints;
	}

	@Override
	public String getPlayerName() 
	{
		return this.playerName;
	}

	@Override
	public void setPlayerName(String playerName)
	{
		this.playerName = playerName;
	}

	@Override
	public int getPoints() 
	{
		return this.points;
	}

	@Override
	public void setPoints(int points) 
	{
		this.points = points;
	}

	@Override
	public String getPlayerId() 
	{
		return this.playerId;
	}

	@Override
	public boolean placeBet(int bet) 
	{
		// check that there are sufficient points to place bet
		if ( bet <= points ){ this.bet = bet; return true; }
		return false;
	}

	@Override
	public int getBet() {
		return bet;
	}

	@Override
	public DicePair getRollResult() {
		return dice;
	}

	@Override
	public void setRollResult(DicePair rollResult) {
		dice = rollResult;	
	}
	
	@Override
	public String toString()
	{
		String tempString = "Player: id= %s, name= %s, points= %s";
		return String.format(tempString , this.playerId, this.playerName ,this.points);
	}
}
