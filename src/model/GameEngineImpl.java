package model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import model.interfaces.DicePair;
import model.interfaces.GameEngine;
import model.interfaces.GameEngineCallback;
import model.interfaces.Player;

public class GameEngineImpl implements GameEngine 
{
	/* init variables and collections */
	private final int BET_RESET = 0;
	private Set<Player> playersList = new HashSet<Player>();
	private Set<GameEngineCallback> gameEngineCallbacksList = new HashSet<GameEngineCallback>();
	private GameEngineCallback gameEngine = new GameEngineCallbackImpl();
	private DicePair houseDiceResult;
	
	
	@Override
	public boolean placeBet(Player player, int bet) {
		// check player has enough points to make bet
		return player.placeBet(bet);
	}

	@Override
	public void rollPlayer(Player player, int initialDelay, int finalDelay, int delayIncrement) {
		/* process rolls */
		DicePair dice = generateDiceRoll();
		for ( ; initialDelay < finalDelay ; initialDelay += delayIncrement)
		{
			threadSleep(initialDelay);
			/* iterate over available game engines */

			dice = generateDiceRoll();

			for (GameEngineCallback gameEngine : gameEngineCallbacksList)
			{
				gameEngine.intermediateResult(player , dice , this);
			}


		}
		/* set players dice result */
		player.setRollResult(dice);
	}
	
	@Override
	public void rollHouse(int initialDelay, int finalDelay, int delayIncrement) {
		/* process rolls */
		DicePair resultDice = generateDiceRoll();
		for ( ; initialDelay < finalDelay ; initialDelay += delayIncrement)
		{
			threadSleep(initialDelay);
			resultDice = generateDiceRoll();
			for (GameEngineCallback gameEngine : gameEngineCallbacksList)
			{	/* print intermediate result */
				gameEngine.intermediateHouseResult(resultDice , this);
			}
		}		
		/* get final roll and process results */

		houseDiceResult = resultDice;

		for (GameEngineCallback gameEngine : gameEngineCallbacksList)
		{
			gameEngine.houseResult(resultDice, this);
		}
		/* process and display final results */
		finalGameResults(resultDice);	
	}

	/* put the thread to sleep */
	private void threadSleep (int delay)
	{	
		try
		{ Thread.sleep(delay); }
		catch(InterruptedException e)
		{ e.printStackTrace(); }
	}
	
	// method for rolling dice
	private DicePair generateDiceRoll ()
	{	/* generate random numbers for dice */
		Random randomGenerator = new Random();
		int dice1 = randomGenerator.nextInt((NUM_FACES - MINIMUM) + 1) + MINIMUM;
		int dice2 = randomGenerator.nextInt((NUM_FACES - MINIMUM) + 1) + MINIMUM;
		DicePair dice = new DicePairImpl(dice1 , dice2 , NUM_FACES);
		return dice;
	}
	
	
	@Override
	public void addPlayer(Player player) {
			playersList.add(player);
	}

	@Override
	public Player getPlayer(String id) {
		/*Iterate over available players and search for Player id */
		for (Player currentPlayer: this.playersList)
		{
			if (currentPlayer.getPlayerId().equals(id))
			{
				return currentPlayer;
			}
		}
		return null;
	}

	@Override
	public boolean removePlayer(Player player) {
		// TODO Auto-generated method stub
		return playersList.remove(player);
	}

	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) {

		// check that gameEngineCallback is not already added */
		if (!gameEngineCallbacksList.contains(gameEngineCallback))
		{
			gameEngineCallbacksList.add(gameEngineCallback);
		}
	}
	
	/* process and display result of game. */
	private void finalGameResults (DicePair houseDiceResult)
	{
		/* calc house total */
		int houseTotal = houseDiceResult.getDice1() + houseDiceResult.getDice2() ;
		/* iterate over players */

		for (Player player : playersList)
		{
			if (player.getBet() != 0)
			{
				int playerTotal = player.getRollResult().getDice1() + player.getRollResult().getDice2();
				if (playerTotal > houseTotal) {
					player.setPoints(player.getPoints() + player.getBet());
				} 
				else if(playerTotal == houseTotal)
				{
					/* If draw ,no points deducted */
				}
				else 
				{
					player.setPoints(player.getPoints() - player.getBet());
				}
				/* display result for player and reset their bet -> display to all game engines */
				for (GameEngineCallback gameEngine : gameEngineCallbacksList) {
					gameEngine.result(player, player.getRollResult(), this);
				}
				player.placeBet(BET_RESET);
			}
		}
		
	}



	@Override
	public boolean removeGameEngineCallback(GameEngineCallback gameEngineCallback) {
	return gameEngineCallbacksList.remove(gameEngineCallback);
	}

	@Override
	public Collection<Player> getAllPlayers() {
		return this.playersList;
	}

	/* return resulting house dice */
	public DicePair getHouseDiceResult() {
		return houseDiceResult;
	}
}
