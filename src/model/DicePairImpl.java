package model;

import model.interfaces.DicePair;

public class DicePairImpl implements DicePair
{
	// instantiate variables
	int dice1 = 0;
	int dice2 = 0;
	int numFaces;
	
	//constructor
	public DicePairImpl(int dice1, int dice2, int numFaces)
	{
		this.dice1 = dice1;
		this.dice2 = dice2;
		this.numFaces = numFaces;
	}

	@Override
	public int getDice1() {
		return dice1;
	}
	
	@Override
	public int getDice2() {
		return dice2;
	}

	@Override
	public int getNumFaces() {
		return numFaces;
	}
	
	@Override
	public String toString(){
		String tempString = "Dice 1: %s,  Dice 2: %s .. Total: %s";
		return String.format(tempString , getDice1() , getDice2() , (getDice1() + getDice2()));
	}
	

	
}
