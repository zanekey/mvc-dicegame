package controller;

import model.GameEngineImpl;
import model.SimplePlayer;
import model.interfaces.Player;
import view.MainFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddPlayerListener implements ActionListener {

   /*locals*/
   GameEngineImpl model;
   MainFrame view;

   /* constructor */
   public AddPlayerListener(GameEngineImpl model , MainFrame view)
   {
      /* instantiate fields */
      this.model = model;
      this.view = view;
   }

   /* handle action performed */
   @Override
   public void actionPerformed(ActionEvent e)
   {
      /* display option pane */
      if ( !view.getGameManager().getPlayerManager().getAddPlayerOptionPane()){ return; }

      /* initialise fields */
      String playerID = generatePlayerId();
      String playerName = view.getGameManager().getPlayerManager().getPlayerName();
      String points = view.getGameManager().getPlayerManager().getPlayerPoints();

      /* check if input is empty */
      boolean emptyInput = (playerName.trim().length() == 0 || points.trim().length()  == 0);
      if (emptyInput)
      {
         this.view.displayerMessage("empty fields detected , input invalid!");
         return;
      }

      /* try converting user string input into Int , catch exception if not */
      int playerPoints = 0;
      try {  playerPoints = Integer.parseInt(points); }
      catch (NumberFormatException exception){ this.view.displayerMessage("Invalid input for Points"); return; }

      /* add player to model and view and display message */
      Player player  = new SimplePlayer(playerID , playerName , playerPoints);
      model.addPlayer(player);

      /* notify and update view */
      this.view.getGameManager().getPlayerManager().addPlayerToComboBox(player);
      this.view.displayerMessage("Player Added Successfully");
      this.view.getGameLogger().logInformationToView(String.format("New player added: %s \n" , player.getPlayerName()));
   }

   /* generate player id */
   private String generatePlayerId()
   {
      /* get number of players */
      int numberOfPlayers = model.getAllPlayers().size();
      /* generate unique id based on number of players avaialble */
      String playerId = Integer.toString(numberOfPlayers + 1);
      return playerId;
   }



}
