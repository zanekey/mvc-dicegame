package controller;

import model.GameEngineImpl;
import model.interfaces.Player;
import view.MainFrame;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class SelectedPlayerListener implements ItemListener {

   /*locals*/
   GameEngineImpl model;
   MainFrame view;

   /* instantiate fields */
   public SelectedPlayerListener (GameEngineImpl model , MainFrame view) { this.model = model; this.view = view; }

   @Override  /* listen for selectedPlayer changes */
   public void itemStateChanged(ItemEvent e) {
      if (e.getStateChange() == ItemEvent.SELECTED) {
         Object item = e.getItem();
         /* update view changes */
         this.view.getStatusBarPanel().setCurrentPlayerLabel( String.format("selected player: %S " , item.toString()));
         this.view.setCurrentSelectedPlayer((Player) e.getItem());
         this.view.getGameLogger().getDicePanel().setPlayerTextLabel(this.view.getCurrentSelectedPlayer().getPlayerName());
      }

   }
}
