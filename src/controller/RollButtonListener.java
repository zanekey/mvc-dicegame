package controller;

import model.GameEngineImpl;
import model.interfaces.Player;
import view.MainFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

public class RollButtonListener implements ActionListener {

   /* set private attributes */
   private GameEngineImpl model;
   private MainFrame view;
   private Collection<Player> copyOfplayers;

   /* constructor */
   public RollButtonListener(GameEngineImpl model , MainFrame view)
   {
      this.model = model;
      this.view = view;
   }

   /* process action performed */
   @Override
   public void actionPerformed(ActionEvent e)
   {
      /* get a copy of avaialble players , to ensure adding a player wont interfere */
      copyOfplayers = new CopyOnWriteArrayList<Player>(model.getAllPlayers());

      /* check to see if any bets have been placed */
      if (!betsPlaced()) { view.displayerMessage("No bets have been placed"); return;}

      /* create dispatch thread and process GUI */
      Thread dispatchThread = new Thread() {
         public void run() {
            /* roll for each player */
            for ( Player player : copyOfplayers)
            {
               /* ensure that only players who have placed a bet will roll */
               if (player.getBet() > 0) {
                  view.setCurrentRollingPlayer(player);
                  view.getStatusBarPanel().setRoundStatus(player.getPlayerName());
                  model.rollPlayer(player, 0, 500, 20);
               }
            }
            /* roll for house */
            view.getStatusBarPanel().setRoundStatus("THE HOUSE");
            model.rollHouse(0, 500, 20);

            /* update view */
            statusBarUpdater();
            /* set current roller to null*/
            view.setCurrentRollingPlayer(null);
            view.getGameManager().getPlayerManager().getComboBox().repaint();
         }
      };
      dispatchThread.start();
   }

   /* log results to view */
   private void logToView(String playerName , int result , int playerPoints , String roundStatus)
   {
      view.getGameLogger().logInformationToView(String.format("%s rolled %s and %s , current Points %s \n" , playerName , result , roundStatus , playerPoints ));
   }

   /* check if any bets have been placed */
   private boolean betsPlaced ()
   {  /* check if any bets have been placed */
      for (Player player : model.getAllPlayers())
      {
         if (player.getBet() > 0){ return true; }
      }
      /* if no bets placed , return false */
      return false;
   }

   /* update status bar */
   private void statusBarUpdater() { view.getStatusBarPanel().resetRoundStatus(); }

}
