package controller;

import model.GameEngineImpl;
import model.SimplePlayer;
import model.interfaces.Player;
import view.MainFrame;

public class PlayerController {
	
	/* instantiate fields */
	private MainFrame view;
	private GameEngineImpl model;

	public PlayerController( GameEngineImpl model , MainFrame view )
	{
		/* initialise fields */
		this.view = view;
		this.model = model;

		/* add listeners to view */
		delegateActionListeners();
	}

	private void delegateActionListeners()
	{
		/* delegate Action Listeners to view */
		this.view.getGameManager().getPlayerManager().addlistenerToButton(new AddPlayerListener(this.model , this.view));
		this.view.getGameManager().getPlayerManager().addComboListener(new SelectedPlayerListener(this.model , this.view));
		this.view.getGameManager().getGameToolbar().addActionListenerToBet(new BetListener(this.model , this.view));
		this.view.getGameManager().getGameToolbar().addActionListenertoRoll(new RollButtonListener(this.model , this.view));
	}

	private void populateGame()
	{
		/* instantiate players */
		Player player = new SimplePlayer("1" , "example" , 500);
		Player player2 = new SimplePlayer("2" , "drongo" , 500);
		Player player3 = new SimplePlayer("3" , "frongo" , 500);

		/* place bet for players */
		player.placeBet(400);
		player2.placeBet(300);
		player3.placeBet(250);

		/* add players to model */
		this.model.addPlayer(player);
		this.model.addPlayer(player2);
		this.model.addPlayer(player3);

		/* add players to view */
		this.view.getGameManager().getPlayerManager().addPlayerToComboBox(player);
		this.view.getGameManager().getPlayerManager().addPlayerToComboBox(player2);
		this.view.getGameManager().getPlayerManager().addPlayerToComboBox(player3);
	}



}
