package controller;

import model.GameEngineImpl;
import view.MainFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BetListener implements ActionListener{

   /*locals*/
   GameEngineImpl model;
   MainFrame view;


   public BetListener(GameEngineImpl model , MainFrame view)
   {
      /* instantiate fields */
      this.model = model;
      this.view = view;
   }

   @Override
   public void actionPerformed(ActionEvent e) {

      /* check current player exists */
      if ( view.getCurrentSelectedPlayer() == null)
      {
         view.displayerMessage("no player currently selected");
         return;
      }

      /* display and process dialog */
      if (view.getGameManager().getGameToolbar().getPlaceBetDialog(view))
      {
         int bet;
         /* get input bet value */
         String inputBet = view.getGameManager().getGameToolbar().getPlaceBetValue();

         /* handle error if non integer values are parsed */
         try {
            bet = Integer.parseInt(inputBet);
         }
         catch (NumberFormatException exception)
         {
            view.displayerMessage("Insufficient points or invalid bet placed");
            return;
         }

         /* ensure bet is valid */
         if(bet > 0 && view.getCurrentSelectedPlayer().placeBet(bet))
         {
            view.displayerMessage("Bet Placed");
            view.getGameLogger().logInformationToView(String.format("Placed %s Bet for %s \n" , view.getCurrentSelectedPlayer().getBet()
                                                                                 , view.getCurrentSelectedPlayer().getPlayerName()));
         }
         else /* else display error message */
         {
            view.displayerMessage("Insufficient points or negative bet placed");
         }
      }

   }




}
