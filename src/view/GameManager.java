package view;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class GameManager extends JPanel {

   private GameToolbar gameToolbar = new GameToolbar();
   private PlayerManager playerManager;

   //Compound border
   private Border compound;
   private Border redline = BorderFactory.createLineBorder(Color.red);

   public GameManager(MainFrame mainFrame)
   {
      /* southPanel , placeholder for panels , compound border*/
      setLayout(new GridLayout(2, 1));
      compound = BorderFactory.createCompoundBorder(redline, compound);
      setBorder(compound);

      playerManager = new PlayerManager(mainFrame);

      /* add panels */
      add(playerManager);
      add(gameToolbar);
   }

   /* get game toolbar */
   public GameToolbar getGameToolbar() {
      return gameToolbar;
   }

   /* get Player Manager */
   public PlayerManager getPlayerManager() {
      return playerManager;
   }
}
