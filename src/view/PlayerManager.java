package view;

import java.awt.*;

import javax.swing.*;

import controller.AddPlayerListener;
import controller.SelectedPlayerListener;
import model.interfaces.Player;

public class PlayerManager extends JPanel {

	/* instantiate required componenets */
	private DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
	private JComboBox<Player> comboBox = new JComboBox<Player>(comboBoxModel);
	private JTextField inputPlayerName = new JTextField();
	private JTextField inputPlayerPoints = new JTextField();
	private JButton addPlayerButton = new JButton("Add Player");
	private JLabel selectPlayerLabel = new JLabel("select Player");
	private JLabel selectedPlayer = new JLabel();
	private MainFrame parentFrame;

	/* constructor */
	public PlayerManager(MainFrame parentFrame)
	{
		this.parentFrame = parentFrame;
		/* set layout */
		setLayout(new GridLayout(1 , 2));
		setBorder(BorderFactory.createRaisedBevelBorder());

		/* sub panel for selecting player */
		JPanel subPanel = new JPanel();
		subPanel.setLayout(new GridLayout(2, 1));

		/* populate subPanel */
		subPanel.add(selectPlayerLabel);
		subPanel.add(comboBox);

		/* populate PlayerManager */
		add(addPlayerButton);
		add(subPanel);
	}

	/* get add player option pane */
	public boolean getAddPlayerOptionPane( )
	{
		/* initialise fields */
		inputPlayerName = new JTextField();
		inputPlayerPoints = new JTextField();

		/* object message for JOptionPane*/
		Object[] message = {"Player Name:", inputPlayerName, "Points:" , inputPlayerPoints, };

		/* option pane , NEEDS TO HANDLE PROPER SELECTION */
		int option = JOptionPane.showConfirmDialog(parentFrame, message, "Add Player", JOptionPane.OK_CANCEL_OPTION);

		/* if user clicks Ok return true */
		if (option == JOptionPane.OK_OPTION ){ return true; }

		/* else return false */
		return false;
	}

	/* retrieve player name */
	public String getPlayerName(){ return this.inputPlayerName.getText(); }

	/* retrieve player points */
	public String getPlayerPoints() {return this.inputPlayerPoints.getText(); }

	/* add listener to add player button */
	public void addlistenerToButton(AddPlayerListener addPlayerListener) { addPlayerButton.addActionListener(addPlayerListener); }

	/*add player to comboBox */
	public void addPlayerToComboBox(Player player) { comboBoxModel.addElement(player); }

	/* add item listener to comboBox */
	public void addComboListener(SelectedPlayerListener listener) { comboBox.addItemListener(listener); }

	/* return selected Player */
	public JLabel getSelectedPlayer() { return selectedPlayer; }
	
	/* return comboBox */
	public JComboBox getComboBox(){ return this.comboBox; }
}
