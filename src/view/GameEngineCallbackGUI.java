package view;

import model.GameEngineImpl;
import model.interfaces.DicePair;
import model.interfaces.GameEngine;
import model.interfaces.GameEngineCallback;
import model.interfaces.Player;

import javax.swing.*;

public class GameEngineCallbackGUI implements GameEngineCallback  {

	/* set private attributes */
	private GameEngineImpl model;
	private MainFrame view;

	public GameEngineCallbackGUI( MainFrame view)
	{
		this.view = view;
	}

	@Override
	public void intermediateResult(Player player, DicePair dicePair, GameEngine gameEngine) {
		/* if selected player is rolling */
		if (view.getCurrentSelectedPlayer() != null && view.getCurrentSelectedPlayer().equals(view.getCurrentRollingPlayer())) {

			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run() {
					/* update view and set player dice accordinginly */
                   view.getGameLogger().getDicePanel().setPlayerDice1(dicePair.getDice1());
                   view.getGameLogger().getDicePanel().setPlayerDice2(dicePair.getDice2());
                   view.getGameLogger().logInformationToView(String.format("ROLLING FOR : -- %s -- dice 1: %s dice 2: %s \n"
                                                 ,player.getPlayerName() , dicePair.getDice1() , dicePair.getDice2() ));
				}
			});
		}
	}

	@Override
	public void result(Player player, DicePair result, GameEngine gameEngine) {

       SwingUtilities.invokeLater(new Runnable()
       {
          @Override
          public void run()
          {
          	 /* update view */
             int playerResult = result.getDice1() + result.getDice2();
             view.getGameLogger().logInformationToView(String.format("%s rolled %s  \n" , player.getPlayerName() , playerResult ));
          }
       });
		
	}

	@Override
	public void intermediateHouseResult(DicePair dicePair, GameEngine gameEngine) {

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
            {
            	/* update view */
				view.getGameLogger().getDicePanel().setHouseDice1(dicePair.getDice1());
				view.getGameLogger().getDicePanel().setHouseDice2(dicePair.getDice2());
				view.getGameLogger().logInformationToView(String.format("ROLLING FOR : -- HOUSE -- dice 1: %s dice 2: %s \n"
                                                                         , dicePair.getDice1() , dicePair.getDice2() ));
			}
		});

	}

	@Override
	public void houseResult(DicePair result, GameEngine gameEngine) {

		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				/* update view */
               int houseResult = result.getDice1() + result.getDice2();
			   view.getGameLogger().logInformationToView(String.format("House rolled %s  \n" , houseResult ));
            }
		});
		
	}

}
