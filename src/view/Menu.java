package view;

import javax.swing.*;

public class Menu extends JMenuBar {

   /* instantiate feilds */
   private JMenuItem play, roll;
   private JMenu fileMenu = new JMenu("File");

   public Menu ()
   {
      /* declare menu Items */
      play = new JMenuItem("Play");
      roll = new JMenuItem("Roll");
      // Adding menu items to menu
      fileMenu.add(play);
      fileMenu.add(roll);
      // adding menu to menu bar
      add(fileMenu);
   }
}
