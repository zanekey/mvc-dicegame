package view;

import javax.swing.*;
import java.awt.*;

public class DicePanel extends JPanel {

   /* instantiate library */
   private ImageLibrary library = new ImageLibrary();

   private ImageIcon playerDiceOneIcon = new ImageIcon(library.getDiceImage(1));
   private ImageIcon playerDiceTwoIcon = new ImageIcon(library.getDiceImage(2));
   private ImageIcon HouseDiceOneIcon = new ImageIcon(library.getDiceImage(1));
   private ImageIcon HouseDiceTwoIcon = new ImageIcon(library.getDiceImage(2));
   private ImageIcon houseIcon = new ImageIcon(library.getHouseImage());
   private ImageIcon playerIcon = new ImageIcon(library.getPlayerImage());

   /* dice labels */
   private JLabel playerDice1 = new JLabel(playerDiceOneIcon , JLabel.CENTER);
   private JLabel playerDice2 = new JLabel( playerDiceTwoIcon , JLabel.CENTER);
   private JLabel houseDice1 = new JLabel( HouseDiceOneIcon , JLabel.CENTER);
   private JLabel houseDice2 = new JLabel( HouseDiceTwoIcon , JLabel.CENTER);

   /* house and player meta info labels */
   private JLabel houseImageLabel = new JLabel( houseIcon , JLabel.CENTER);
   private JLabel playerImageLabel = new JLabel( playerIcon , JLabel.CENTER);
   private JLabel houseTextLabel = new JLabel( "<html><font size= '4' color='white'>HOUSE</font></html>" );
   private JLabel playerTextLabel = new JLabel(  "<html><font size= '4' color='white'>PLAYER</font></html>");

   private Color purple = new Color(102, 34, 153);
   private Color darkRed = new Color(204, 0, 0);

   public DicePanel ()
   {
      /* get sub panels for house and player */
      JPanel playerPanel = getSubPanel(playerTextLabel, playerImageLabel, playerDice1 , playerDice2 );
      JPanel housePanel = getSubPanel(houseTextLabel , houseImageLabel, houseDice1 , houseDice2);

      /* set settings */
      this.setLayout(new GridLayout(2,1));
      playerPanel.setBackground(purple);
      housePanel.setBackground(darkRed);

      /* add panels */
      add(playerPanel);
      add(housePanel);
   }

   /* dice image setters */
   /* set dice 1 */
   public void setPlayerDice1(int diceValue) {
      this.playerDice1.setIcon(new ImageIcon(library.getDiceImage(diceValue)));
   }

   /* set player dice 2 */
   public void setPlayerDice2(int diceValue) {
      this.playerDice2.setIcon(new ImageIcon(library.getDiceImage(diceValue)));
   }

   /* set house dice 1 */
   public void setHouseDice1(int diceValue) {
      this.houseDice1.setIcon(new ImageIcon(library.getDiceImage(diceValue)));
   }

   /* set house dice 2 */
   public void setHouseDice2(int diceValue) {
      this.houseDice2.setIcon(new ImageIcon(library.getDiceImage(diceValue)));
   }

   /* main player and house Labels creator */
   private JPanel getSubPanel (JLabel label , JLabel image , JLabel dice1 , JLabel dice2 )
   {
      /* add attributes to subPanel */
      JPanel subPanel = new JPanel(new GridLayout(1,4 ));
      subPanel.add(label);
      subPanel.add(image);
      subPanel.add(dice1);
      subPanel.add(dice2);
      return subPanel;
   }

   /* set player text label */
   public void setPlayerTextLabel(String playerName) {
      this.playerTextLabel.setText(String.format("<html><font size= '4' color='white'> %s </font></html>" , playerName));
   }
}
