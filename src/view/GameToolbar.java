package view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GameToolbar extends JPanel {

	/* initiailise attributes */
	private JButton placeBet = new JButton("Place Bet");
	private JButton rollDice = new JButton("Roll");
	private JToolBar toolbar = new JToolBar();
	private String placeBetValue;

	public GameToolbar()
	{
		/* set layout */
		setLayout(new GridLayout());
		toolbar.setBorder(BorderFactory.createRaisedBevelBorder());
		toolbar.setLayout(new GridLayout(1 , 2));

		/* add buttons to toolbar */
		toolbar.add(placeBet);
		toolbar.add(rollDice);
		add(toolbar);
	}

	/* place bet option pane */
	public boolean getPlaceBetDialog(MainFrame parentFrame)
	{
		/* init values */
		JTextField placeBet =  new JTextField();

		/* object message for option pane */
		Object[] message = { "Bet:", placeBet, };

		/* option pane */
		int option = JOptionPane.showConfirmDialog(parentFrame, message, "Place Bet", JOptionPane.OK_CANCEL_OPTION);

		/* process user decision and ensure bet is > 0 */
		if (option == JOptionPane.OK_OPTION) {
			/* set bet value */
			this.placeBetValue = placeBet.getText();
			return true;
		}

		/* else return false */
		return false;
	}
	/* add action listener to bet button */
	public void addActionListenerToBet(ActionListener betListener) { placeBet.addActionListener(betListener); }
	/* return placed bet */
	public String getPlaceBetValue() { return placeBetValue; }

	/* add action listener to roll button */
	public void addActionListenertoRoll(ActionListener rollListener)
	{
		rollDice.addActionListener(rollListener);
	}
}
