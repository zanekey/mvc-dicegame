package view;

import javax.swing.*;
import java.awt.*;

public class StatusBarPanel extends JPanel {

   private JLabel currentPlayerLabel = new JLabel("Selected Player: ");
   private JLabel rollerLabel = new JLabel();

   /* status bar */
   public StatusBarPanel ()
   {
      setLayout(new GridLayout(1 ,2 ));
      add(currentPlayerLabel);
      add(rollerLabel);
      setBorder(BorderFactory.createRaisedSoftBevelBorder());
   }

   /* set current player */
   public void setCurrentPlayerLabel(String newText) {
      this.currentPlayerLabel.setText(newText);
   }

   /* set the rolling label */
   public void setRollerLabel(String newText) {
      this.rollerLabel.setText(newText);
   }

   /* reset round status */
   public void resetRoundStatus()
   {
      this.rollerLabel.setText("");
   }

   /* update round status for when player is rolling */
   public void setRoundStatus(String player) {
      this.rollerLabel.setText(String.format("<html><font size= '5' color='orange'>Rolling for %s </font></html>" , player));
   }
}
