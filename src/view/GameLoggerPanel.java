package view;

import javax.swing.*;
import java.awt.*;

public class GameLoggerPanel extends JPanel {

   /* declare fields */
   private JTextArea gameLogger = new JTextArea();
   private JScrollPane scrollPane = new JScrollPane(gameLogger);
   private DicePanel dicePanel = new DicePanel();
   private Color gold = new Color(255, 204, 51);

   /* constructor */
   public GameLoggerPanel()
   {
      /* set settings */
      setLayout(new GridLayout(1, 2 , 5,0));
      gameLogger.setEditable(false);
      gameLogger.setBackground(gold);

      gameLogger.setFont(gameLogger.getFont().deriveFont(16f));
      this.gameLogger.setForeground(Color.blue);

      /*add components to panel */
      add(scrollPane);
      add(dicePanel);
   }

   /* add text to logger */
   public void logInformationToView(String loggingText) {
      this.gameLogger.append(loggingText);
   }

   /* get dicePanel*/
   public DicePanel getDicePanel() {
      return dicePanel;
   }
}
