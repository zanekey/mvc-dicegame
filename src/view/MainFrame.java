package view;

import java.awt.*;
import javax.swing.*;

import model.interfaces.Player;

public class MainFrame extends JFrame  {

	/* instantiate fields */
	private GameLoggerPanel gameLogger;
	private StatusBarPanel statusBarPanel;
	private GameManager gameManager;
	private Player currentSelectedPlayer = null;
	private Player currentRollingPlayer = null;

	//minimum dimension size
	private Dimension minimumSize = new Dimension(840, 400);
	public MainFrame ()
	{
		/* set settings , drop-down menu and border*/
		setLayout(new BorderLayout());
		setMinimumSize(minimumSize);
		setLocationRelativeTo(null);
		setJMenuBar(new Menu());

		/* instantiate panels */
		gameLogger = new GameLoggerPanel();
		statusBarPanel = new StatusBarPanel();
		gameManager = new GameManager(this);

		/* add panels to mainframe */
		add(statusBarPanel, BorderLayout.NORTH);
		add(gameManager , BorderLayout.SOUTH);
		add(gameLogger, BorderLayout.CENTER);

		/* declare Settings DOUBLE CHECK ____!!!! */
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SADI_DICE_GAME");
		setVisible(true);
	}

	/* display message */
	public void displayerMessage(String message){ JOptionPane.showMessageDialog(this , message); }

	/* get statusBar Panel */
	public StatusBarPanel getStatusBarPanel() { return this.statusBarPanel; }

	/* get gameManager */
	public GameManager getGameManager() { return gameManager; }

	/* get gameLogger panel */
	public GameLoggerPanel getGameLogger() { return gameLogger; }

	/* get currently selected Player */
	public Player getCurrentSelectedPlayer() { return currentSelectedPlayer; }

	/* get currently selected Player */
	public Player getCurrentRollingPlayer() { return currentRollingPlayer; }

	/* set currently selected Player */
	public void setCurrentSelectedPlayer(Player currentSelectedPlayer) { this.currentSelectedPlayer = currentSelectedPlayer; }

	/* set current Rolling Player */
	public void setCurrentRollingPlayer(Player currentRollingPlayer) { this.currentRollingPlayer = currentRollingPlayer; }
}
