package view;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;

public class ImageLibrary {

   /* NOTE: IMAGES USED IN THIS GAME ARE ALL PROPERTY OF http://game-icons.net/tags/dice.html */
   /* images paths */
   public String dice1 = "images/one.PNG";
   public String dice2 = "images/two.PNG";
   public String dice3 = "images/three.PNG";
   public String dice4 = "images/four.PNG";
   public String dice5 = "images/five.PNG";
   public String dice6 = "images/six.PNG";
   public String house = "images/theHouse.PNG";
   public String player = "images/thePlayer.PNG";

   /* image paths */
   String[] diceImagesPath = {dice1 , dice2 , dice3 , dice4 , dice5 , dice6 };
   String[] miscPicsPath = {house , player};

   /* init images */
   Image diceImage1 = null;
   Image diceImage2 = null;
   Image diceImage3 = null;
   Image diceImage4 = null;
   Image diceImage5 = null;
   Image diceImage6 = null;
   Image houseImage = null;
   Image playerImage = null;

   /* init images */
   Image[] diceImages = {diceImage1 , diceImage2 , diceImage3 , diceImage4 , diceImage5 , diceImage6};
   Image[] miscImages = {houseImage , playerImage };

   public ImageLibrary()
   {
      /* load images */
      loadImages();
   }

   /* get dice image */
   public Image getDiceImage (int diceValue)
   {
      int imageIndex = diceValue - 1;
      return diceImages[imageIndex];
   }

   /* load Images */
   public void loadImages()
   {
      int i = 0;
      while ( i < diceImages.length)
      {
         try {
               diceImages[i] = ImageIO.read( new FileInputStream(diceImagesPath[i]) );
               diceImages[i] = diceImages[i].getScaledInstance(100, 100 , diceImages[i].SCALE_DEFAULT );

         } catch (IOException e) {
            e.printStackTrace();
         }
         i++;
      }

      i = 0;
      while ( i < miscImages.length)
      {
         try {
            miscImages[i] = ImageIO.read( new FileInputStream(miscPicsPath[i]) );
            miscImages[i] = miscImages[i].getScaledInstance(120, 120 , miscImages[i].SCALE_DEFAULT );
         } catch (IOException e) {
            e.printStackTrace();
         }

         i++;
      }
   }

   /* get house image */
   public Image getHouseImage() {
      return miscImages[0];
   }

   /* get player Image */
   public Image getPlayerImage() {
      return miscImages[1];
   }
}